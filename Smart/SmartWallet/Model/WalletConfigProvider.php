<?php
/**
 * Copyright © BrainActs Commerce OÜ. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Smart\SmartWallet\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\DB\Select;

/**
 * Class RewardConfigProvider
 *
 * @author BrainActs Core Team <support@brainacts.com>
 */
class WalletConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var \BrainActs\RewardPoints\Model\ResourceModel\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * @var \BrainActs\RewardPoints\Helper\Data
     */
    private $pointsHelper;

    /**
     * @var null
     */
    private $points = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \BrainActs\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory
     * @param \BrainActs\RewardPoints\Helper\Data $pointsHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        \BrainActs\RewardPoints\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        \BrainActs\RewardPoints\Helper\Data $pointsHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->historyCollectionFactory = $historyCollectionFactory;
        $this->pointsHelper = $pointsHelper;
    }

    /**
     * @return array|mixed
     */
    public function getConfig()
    {
        $output['wallet'] = [
            'amount' => 10,
            'selected_points' => 20,
            'available_points' => 30,
            'max_points' => 40,
            'exchange' => 50,
            'is_visible' => 60
        ];
        return $output;
    }
}
