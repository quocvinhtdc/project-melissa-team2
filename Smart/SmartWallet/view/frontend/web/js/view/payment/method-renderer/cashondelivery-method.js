/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define([
    'Magento_Checkout/js/view/payment/default',
    'Smart_SmartWallet/js/model/payment/smart-wallet-messages',
    // 'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Ui/js/model/messageList'
], function (Component, messageContainer, fullScreenLoader, ) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Smart_SmartWallet/payment/cashondelivery'
        },
        /*r
               initObservable: function () {
                   this._super()
                       .observe([
                           'allbank',
                           'activecashondelivery'
                       ]);
                   return this;
               },*/
        getData: function() {
            return {
                'method': this.item.method,
                'additional_data': {
                    'cashondelivery': jQuery('#cashondelivery_smartwallet').val()
                }
            };
        },
        /**
         *  wallet validation
         *
         * @returns {Boolean}
         */
        validate: function () {
            var form = '#smart-wallet-form';
            return jQuery(form).validation() && jQuery(form).validation('isValid');
        },
        /**
         *  wallet validation
         *
         * @returns {Boolean}
         */
        apply: function () {
            messageContainer.addSuccessMessage({ messages: 'Reward points was successfully applied' });
            if(this.validate()) {
                messageContainer.startLoader();
            }
            // var point = jQuery('#cashondelivery_smartwallet').val(),
            //     max_points = window.checkoutConfig.wallet.max_points,
            //     message = 'Reward points was successfully applied.';
            //     if(100 > max_points){
            //         fullScreenLoader.startLoader();
            //     }else {
            //
            //     }

        },
        /**
         * get text customer wallet
         * @returns {*}
         */
        getCurrentPointsText: function () {
            return  window.checkoutConfig.wallet.max_points;
        },
        /**
         * Returns payment method instructions.
         *
         * @return {*}
         */
        getInstructions: function () {
            return window.checkoutConfig.payment.instructions[this.item.method];
        }
    });
});
